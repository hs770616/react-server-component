import { createRoot } from "react-dom";
import App from "./App.client";

const initialCache = new Map();
const app = createRoot(document.getElementById("root"));
app.render(<App initialCache={initialCache} />);
